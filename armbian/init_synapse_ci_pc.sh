#!/bin/sh

cdir=$(pwd)
u=${USER}

sudo ls >> /dev/null

cp conf/armbian/bashrc ~/.bashrc
cp conf/armbian/profile ~/.profile
cp conf/armbian/dircolors ~/.dircolors
sudo cp conf/armbian/git /usr/share/bash-completion/completions/git

sudo apt update -y
sudo apt upgrade -y

sudo apt install htop psmisc git curl wget build-essential -y

mkdir ~/.ssh
echo "ForwardAgent yes" > ~/.ssh/config
echo "StrictHostKeyChecking no" >> ~/.ssh/config


# Docker & GITLAB CI


# Synapse


## TERRARIA SERVER - TO TEST
# sudo apt-get install mono-complete screen
# cd
# wget https://terraria.org/api/download/pc-dedicated-server/terraria-server-1449.zip
# unzip terraria-server*
# cd ${cdir}
# sudo cp conf/armbian/terraria-server.service /lib/systemd/system/terraria-server.service
# sudo cp conf/armbian/terraria-server.sh /usr/bin/terraria-server.sh
# sudo chmod +x /usr/bin/terraria-server.sh
# sudo chmod a+w /var/log
# ln -s /home/${USER}/1449/Linux/ /home/${USER}/Terraria-server
# sudo systemctl enable terraria-server.service

## generate terraria world
# mono --server --gc=sgen -O=all /home/${USER}/Terraria-server/TerrariaServer.exe \
# -port 7777 \
# -players 16 \
# -pass P@ssW0rd

cd /usr/bin
sudo ln -sf python3 python
cd

sudo apt autoremove
