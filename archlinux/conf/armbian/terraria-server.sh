#!/bin/sh

mono --server --gc=sgen -O=all /home/${USER}/Terraria-server/TerrariaServer.exe \
-port 7777 \
-players 16 \
-pass P@ssW0rd \
-world /home/${USER}/.local/share/Terraria/Worlds/World.wld \
> /var/log/terraria-server.log
