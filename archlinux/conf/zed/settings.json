// Zed settings
//
// For information on how to configure Zed, see the Zed
// documentation: https://zed.dev/docs/configuring-zed
//
// To see all of Zed's default settings without changing your
// custom settings, run the `open default settings` command
// from the command palette or from `Zed` application menu.
{
  "features": {
    "edit_prediction_provider": "zed"
  },
  "assistant": {
    "default_model": {
      "provider": "zed.dev",
      "model": "claude-3-5-sonnet-latest"
    },
    "version": "2"
  },
  "theme": "One Dark",
  "ui_font_size": 15,
  "scroll_sensitivity": 2.0,
  "buffer_font_size": 13.9,
  "hour_format": "hour24",
  "autosave": "off",
  "working_directory": "current_project_directory",
  "vim_mode": false,

  "tab_bar": {
    "show": true,
    "show_nav_history_buttons": false
  },
  "project_panel": {
    "button": true,
    "dock": "left",
    "git_status": true,
    "default_width": 300
  },
  "collaboration_panel": {
    "button": false
  },
  "chat_panel": {
    "button": false
  },
  "notification_panel": {
    "dock": "left"
  },

  "tab_size": 2,
  "hard_tabs": true,
  "soft_wrap": "preferred_line_length",
  "preferred_line_length": 100,
  "show_whitespaces": "boundary",
  "extend_comment_on_newline": false,
  "indent_guides": {
    "enabled": true,
    "line_width": 1,
    "active_line_width": 1,
    "coloring": "indent_aware",
    "background_coloring": "disabled"
  },
  "remove_trailing_whitespace_on_save": true,
  "always_treat_brackets_as_autoclosed": false,
  "file_types": {
    "Dockerfile": ["Dockerfile*"],
    "Shell Script": [".env.*", ".flaskenv", ".flaskenv.*"]
  },
  "format_on_save": "off",
  "file_scan_exclusions": [
    "**/*.map",
    "**/*.pyc",
    "**/*.git",
    "**/*__pycache__",
    "**/*.pytest_cache",
    "**/*.ruff_cache",
    "**/*.egg-info"
  ],

  "show_completions_on_input": true,
  "show_completion_documentation": true,
  "inlay_hints": {
    "enabled": false,
    "show_type_hints": true,
    "show_parameter_hints": true,
    "show_other_hints": true,
    "edit_debounce_ms": 700,
    "scroll_debounce_ms": 50
  },

  "git": {
    "git_gutter": "tracked_files",
    "inline_blame": {
      "enabled": true
    }
  },

  "languages": {
    "Python": {
      "language_servers": ["pyright", "ruff"],
      "preferred_line_length": 79,
      "format_on_save": "off"
    }
  },

  "auto_install_extensions": {
    "html": true,
    "toml": true,
    "sql": true,
    "ruff": true,
    "glsl": true,
    "wgsl": true,
    "dockerfile": true,
    "docker-compose": true,
    "rainbow-csv": true,
    "git-firefly": true
  },

  "experimental.theme_overrides": {
    "background": "#0b0c0f",
    "editor.background": "#0b0c0f",
    "title_bar.background": "#14161a",
    "toolbar.background": "#1d2024",
    "terminal.background": "#000",
    "scrollbar.thumb.background": "#28292b",
    "scrollbar.thumb.border": "#28292b",
    "tab_bar.background": "#121418",
    "tab.inactive_background": "#121418",
    "tab.active_background": "#1d2024",
    "status_bar.background": "#0e1013",
    "editor.active_line.background": "#14161a",
    "editor.gutter.background": "#0e1013",
    "scrollbar.track.border": "#0b0c0f",
    "surface.background": "#16181cff",
    "panel.background": "#16181c",
    "ghost_element.hover": "#0f1115",
    "ghost_element.selected": "#0e1013",

    "players": [
      {
        "cursor": "#607eb9",
        "selection": "#284379"
      }
    ],
    "border": "#191c21",
    "syntax": {
      "comment": {
        "font_style": "italic"
      },
      "comment.doc": {
        "font_style": "italic"
      }
    }
  }
}
