#!/bin/sh

if [ `whoami` != "root" ]; then
    echo "Run this script as root."
    exit
fi
bd=`pwd`

ARGUMENT_LIST=(
    "dir"
    "boot-partition"
)
opts=$(getopt \
    --longoptions "$(printf "%s:," "${ARGUMENT_LIST[@]}")" \
    --name "$(basename "$0")" \
    --options "" \
    -- "$@"
)
eval set --$opts
while [[ $# -gt 0 ]]; do
    case $1 in
        --dir)
            dir=$2
            shift 2
            ;;

        --domain)
            domain=$2
            shift 2
            ;;

        --admin)
            admin=$2
            shift 2
            ;;

        --password)
            password=$2
            shift 2
            ;;

        --upgrade)
            upgrade="Y"
            shift 1
            ;;
        *)
            break
            ;;
    esac
done

cd ${dir}

if [[ ! -z "${upgrade}" ]]; then
    source venv/bin/activate
    python3 -m pip --upgrade install matrix-synapse mautrix-facebook[all]
else
    if [ -f "/etc/arch-release" ]; then
        pacman --noconfirm -S certbot python-pip python-virtualenv python-setuptools nginx certbot-nginx
    else
        apt-get install python3-pip python3-virtualenv python3-setuptools python3-certbot-nginx nginx certbot -y
    fi
    mkdir ./bridges
    python3 -m venv venv
    source venv/bin/activate

    python3 -m pip --upgrade install matrix-synapse mautrix-facebook[all]

    # certbot
    certbot --nginx -d ${domain}
    killall nginx
    cp ./nginx.conf /etc/nginx/nginx.conf

    # copy bridge configs
    cp ./synapse_configs/* ./bridges

    python -m synapse.app.homeserver \
    --server-name ${domain} \
    --config-path homeserver.yaml \
    --generate-config \
    --report-stats=no

    # update path in services

    # generate registrations
    sed -i "s/__USER__/${admin}/g" ./bridges/*
    sed -i "s/__DOMAIN__/${domain}/g" ./bridges/*
    sed -i "s/__PATH__/${dir}/g" ./bridges/*

    python -m mautrix_facebook -c ./bridges/messenger_config.yaml -r ./bridges/messenger_registration -g

    # create systemd services
    sed -i "s/__PATH__/${dir}/g" ./services/*
    cp ./services/* /etc/systemd/system/

    # enable systemd services
    systemctl enable synapse.service
    systemctl enable maultrix_messenger.service
    systemctl enable maultrix_slack.service
    systemctl enable maultrix_telegram.service
    systemctl enable maultrix_signal.service
    systemctl enable nginx.service

    systemctl start synapse.service
    sleep 5
    register_new_matrix_user -u ${admin} -p ${password} -c homeserver.yaml -a

    # bridge links into main config
    echo "app_service_config_files:" >> ./homeserver.yaml
    echo "- ${dir}/bridges/messenger_registration.yaml" >> ./homeserver.yaml
    echo "- ${dir}/bridges/slack_registration.yaml" >> ./homeserver.yaml
    echo "- ${dir}/bridges/telegram_registration.yaml" >> ./homeserver.yaml
    echo "- ${dir}/bridges/signal_registration.yaml" >> ./homeserver.yaml

    shutdown -r 0
fi

cd ${bd}











bd=`pwd`
domain=${1}
user=${2}
if [ `whoami` != "root" ]; then
    echo "Run this script as root."
    exit
fi
if [ ! -z "${4}" ]; then
    type=${4}
else
    type="pip"
fi
if [ ! -z "${5}" ]; then
    directory=${5}
else
    directory=`pwd`
fi

if [ "${type}" = "pip" ]; then
    cd directory
    python -m venv venv
    source venv/bin/activate
    pip install matrix-synapse
    python -m synapse.app.homeserver \
        --server-name ${domain} \
        --config-path synapse.yaml \
        --generate-config \
        --report-stats=no
    cp -f ./matrix_synapse.yaml ./synapse.yaml
    sed -i "s/__DOMAIN__/${domain}/" ./synapse.yaml
    synctl start
    register_new_matrix_user -c synapse.yaml -u ${user} -a http://127.0.0.1:8008
elif [ "${type}" = "pacman" ]; then
    pacman -S matrix-synapse certbot
    cd /var/lib/synapse
    sudo -u synapse python -m synapse.app.homeserver \
        --server-name ${domain} \
        --config-path synapse.yaml \
        --generate-config \
        --report-stats=no

     systemctl enable synapse.service
     systemctl start synapse.service
     register_new_matrix_user -c synapse.yaml -u ${user} -a http://127.0.0.1:8008
else
    apt install matrix-synapse-py3
    apt install certbot nginx certbot-nginx -y
    cd /var/lib/synapse

    systemctl enable synapse.service
    systemctl start synapse.service
fi

certbot certonly --nginx -d ${domain}
cp -f ./matrix_nginx.conf /etc/nginx.conf
sed -i "s/__DOMAIN__/${domain}/" /etc/nginx.conf

systemctl enable nginx.service
systemctl start nginx.service

cd $bd
