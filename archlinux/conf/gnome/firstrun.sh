#!/bin/bash

mkdir ~/.config/wallpapers
mv ~/Pictures/lilith.png ~/.config/wallpapers

#gnome-extensions enable vertical-overview@RensAlthuis.github.com

dconf load / < ~/.gnome.conf
rm -rf ~/.gnome.conf

gsettings set org.gnome.desktop.background picture-uri file:///home/${USER}/.config/wallpapers/lilith.png
gsettings set org.gnome.desktop.screensaver picture-uri file:///home/${USER}/.config/wallpapers/lilith.png

gsettings set org.gnome.shell.app-switcher current-workspace-only true
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "['']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-down "['<Control><Super>Down']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-up "['<Control><Super>Up']"

sudo sed -i s/"AutomaticLogin"/"# AutomaticLogin"/ /etc/gdm/custom.conf

while ! ping -c 1 -n -w 1 www.google.com &> /dev/null
do
    printf "Waiting for network connection\n"
    sleep 3
done

# Docker db images prepare
cd ~/.custom_images/postgres
docker-compose build
docker-compose create
cd ~/.custom_images/mysql
docker-compose build
docker-compose create
cd ~


# cleanup & sudoers fix
sudo rm -rf /first_boot.sh
sudo rm -rf ~/.config/autostart/first_boot-autostart.desktop
sudo mv ./sudoers2 /etc/sudoers
sudo su -c "echo '%wheel ALL=(ALL) ALL' > /etc/sudoers"

touch ~/.histfile


# login screen keymap
sudo localectl set-keymap cz
sudo localectl set-x11-keymap cz, us

sleep 1
reboot
