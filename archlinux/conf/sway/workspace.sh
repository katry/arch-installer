#!/bin/sh

index=0
if [ -s /tmp/sway_workspace ] ; then
    workspace=$(cat /tmp/sway_workspace)
    loaded=1
else
    workspace=0
    loaded=0
fi
current=0
lst=0

for i in $(swaymsg -t get_workspaces | egrep "num|focused")
do
    index=$((index+1))
    if [ "$((index%4))" == "0" ] ; then
        if [ "${current}" == "0" ] && [ ${loaded} == "0"  ] ; then
            workspace=$((workspace+1))
        fi
        if [ "$i" == "true,"  ] ; then
            current=1
        fi
    elif [ "$((index%2))" == "0" ] ; then
        lst="${i//,/}"
    fi
done

if [ "${1}" == "next" ] ; then
   workspace=$((workspace+1))
elif [ "${1}" == "prev"  ] ; then
   if [ "$workspace" == "1" ] ; then
        workspace=$lst
   else
        workspace=$((workspace-1))
   fi
fi
echo $workspace > /tmp/sway_workspace
if [ "${2}" == "move" ] ; then
   swaymsg move container to workspace $workspace
   swaymsg workspace $workspace
else
   swaymsg workspace $workspace
fi

