#!/usr/bin/env python
from sys import argv
from i3ipc import Connection
from setproctitle import setproctitle

setproctitle("app-switcher")


class WS:
	i3 = Connection()
	workspaces = i3.get_workspaces()
	outputs = i3.get_outputs()

	@staticmethod
	def get_position():
		actual = int(WS.i3.get_tree().find_focused().workspace().name)
		print(actual, len(WS.outputs))
		if len(WS.outputs) == 1:
			return actual
		while actual % len(WS.outputs) != 1:
			actual -= 1
		return actual

	def prev(move=False):
		position = WS.get_position()
		diff = int(WS.i3.get_tree().find_focused().workspace().name) - position
		if position == 1:
			return
		position -= len(WS.outputs)
		i = 0
		while i < len(WS.outputs):
			if i == diff and move:
				WS.i3.command("move container to workspace %s" % (i + position))
			WS.i3.command("workspace %s" % (i + position))
			i += 1

	@staticmethod
	def next(move=False):
		position = WS.get_position()
		diff = int(WS.i3.get_tree().find_focused().workspace().name) - position
		if position + len(WS.outputs) == len(WS.workspaces) + 1:
			empty = True
			for output in WS.outputs:
				w_id = output.ipc_data["current_workspace"]
				for workspace in WS.workspaces:
					print(workspace.name, workspace.ipc_data["representation"])
					if (
						workspace.name == w_id and
						workspace.ipc_data["representation"] not in (None, "H[]", "H[T[]]") and
						(not move or " " in workspace.ipc_data["representation"])
					):
						empty = False
			if empty:
				return
			WS.create()
		i = 0
		while i < len(WS.outputs):
			i += 1
			if diff + 1 == i and move:
				print("moving")
				res = WS.i3.command("move container to workspace %s" % (i + position))[0]
				print(vars(res))
			WS.i3.command("workspace %s" % (i + position))

	def create():
		number = len(WS.workspaces) + 1
		i = 0
		while i < len(WS.outputs):
			WS.i3.command("workspace %s output %s" % (number + i, WS.outputs[i].name))
			i += 1


if __name__ == "__main__":
	if str(argv[1]) == 'prev':
		WS.prev()
	elif str(argv[1]) == 'next':
		WS.next()
	elif str(argv[1]) == 'move-prev':
		WS.prev(True)
	elif str(argv[1]) == 'move-next':
		WS.next(True)
