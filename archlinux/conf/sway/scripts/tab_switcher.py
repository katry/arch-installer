#!/usr/bin/env python
from random import randint
from time import sleep
from sys import argv, exit
from os import system
from json import loads, dumps
from i3ipc import Connection
from setproctitle import setproctitle
from psutil import process_iter


class Switcher:
    socket = None

    @staticmethod
    def save_changes(seed, recursive=60):
        s = open("/tmp/.tab-daemon-seed", "r")
        rs = s.read()
        s.close()
        if rs != seed:
            return
        i3 = Connection()
        tree = i3.get_tree()
        w = tree.find_focused().workspace()
        output = w.ipc_data["output"]
        status = Switcher.__load(output, str(w.id))
        candidate = status.pop("candidate", None)
        if candidate:
            status[output][str(w.id)]["apps"].remove(candidate["app"])
            status[output][str(w.id)]["apps"].insert(0, candidate["app"])
            status[output][str(w.id)]["windows"][candidate["app"]].remove(candidate["window"])
            status[output][str(w.id)]["windows"][candidate["app"]].insert(0, candidate["window"])
            Switcher.__save(status)
            tree.find_by_id(candidate["window"]).command("exec foot echo 1")
        else:
            if recursive:
                sleep(0.05)
                Switcher.save_changes(seed, recursive-1)
                return
        system("killall window-choose-list")

    @staticmethod
    def load():
        i3 = Connection()
        tree = i3.get_tree()
        focused = tree.find_focused()
        if (focused.app_id or focused.window_class)[-9:] == "-dropdown":
            exit()
        Switcher.active = {
            "app": focused.app_id or focused.window_class,
            "id": focused.id
        }
        ws = focused.workspace()
        output = ws.ipc_data["output"]
        status = Switcher.__load(output, str(ws.id))
        if output not in status:
            status[output] = {}
        if str(ws.id) not in status[output]:
            status[output][str(ws.id)] = {
                "apps": [],
                "windows": {}
            }
        for i in ws.descendants():
            if not i.app_id and not i.window_class:
                continue
            app = i.app_id or i.window_class
            if app[-9:] == "-dropdown":
                continue
            if app not in status[output][str(ws.id)]["apps"]:
                status[output][str(ws.id)]["apps"].append(app)
                status[output][str(ws.id)]["windows"][app] = [i.id]
            elif i.id not in status[output][str(ws.id)]["windows"][app]:
                status[output][str(ws.id)]["windows"][app].append(i.id)
        for a in status[output][str(ws.id)]["apps"].copy():
            for w in status[output][str(ws.id)]["windows"][a].copy():
                if not ws.find_by_id(w):
                    status[output][str(ws.id)]["windows"][a].remove(w)
                    if not len(status[output][str(ws.id)]["windows"][a]):
                        status[output][str(ws.id)]["apps"].remove(a)
        return status, tree, output, str(ws.id)

    @staticmethod
    def change_app(reverse=False, initializing=False):
        status, tree, output, w = Switcher.load()
        app = Switcher.active["app"]
        index = status[output][w]["apps"].index(app)
        if reverse:
            index = index - 1 if index > 0 else len(status[output][w]["apps"]) - 1
        else:
            index = index + 1 if index < len(status[output][w]["apps"]) - 1 else 0
        system("echo '\nactual %s' >> /tmp/tablog " % app)
        system("echo 'apps %s' >> /tmp/tablog " % ",".join(status[output][w]["apps"]))
        system("echo 'index %s' >> /tmp/tablog " % index)
        new_focus = status[output][w]["apps"][index]
        tree.find_by_id(status[output][w]["windows"][new_focus][0]).command("focus")
        status["candidate"] = {
            "app": new_focus,
            "window": status[output][w]["windows"][new_focus][0]
        }
        Switcher.__save(status)

    @staticmethod
    def change_window(reverse=False, initializing=False):
        status, tree, output, w = Switcher.load()
        app = Switcher.active["app"]
        window = Switcher.active["id"]
        index = status[output][w]["windows"][app].index(window)
        if reverse:
            index = index - 1 if index > 0 else len(status[output][w]["windows"][app]) - 1
        else:
            index = index + 1 if index < len(status[output][w]["windows"][app]) - 1 else 0
        new_focus = status[output][w]["windows"][app][index]
        tree.find_by_id(new_focus).command("focus")
        status["candidate"] = {
            "app": app,
            "window": new_focus
        }
        Switcher.__save(status)

    @staticmethod
    def __load(output, workspace_id):
        try:
            switcher_file = open("/tmp/.tab-switcher-status", "r")
            status = loads(switcher_file.read())
            switcher_file.close()
        except Exception:
            status = {
                output: {
                    workspace_id: {
                        "apps": [],
                        "windows": {}
                    }
                }
            }
        return status

    @staticmethod
    def __save(status: dict):
        switcher_file = open("/tmp/.tab-switcher-status", "w")
        switcher_file.write(dumps(status))
        switcher_file.close()


if __name__ == "__main__":
    chooser = False
    saver = False
    for proc in process_iter():
        name = proc.as_dict(attrs=["name"])["name"]
        if name == "window-choose-list":
            chooser = True
            if saver:
                break
        if name == "app-switcher-saver":
            saver = True
            if chooser:
                break
    if not saver:
        system("nohup ~/.config/sway/scripts/tab_switcher.py save-daemon > /dev/null &")

    direction = False
    if len(argv) > 2:
        direction = argv[2] == "reverse"

    if str(argv[1]) == 'switch-app':
        setproctitle("app-switcher")
        Switcher.change_app(direction)
    elif str(argv[1]) == 'switch-window':
        setproctitle("app-switcher")
        if not chooser:
            system("nohup ~/.config/sway/scripts/chooser.py > /dev/null &")
        Switcher.change_window(direction)

    elif str(argv[1]) == 'save-daemon':
        from evdev import InputDevice
        from select import select
        setproctitle("app-switcher-saver")
        dev = InputDevice('/dev/input/by-path/platform-i8042-serio-0-event-kbd')
        while True:
            select([dev], [], [])
            for event in dev.read():
                if event.code == 56 and event.value == 0:
                    seed = str(randint(1E3, 1E12))
                    s = open("/tmp/.tab-daemon-seed", "w")
                    s.write(seed)
                    s.close()
                    Switcher.save_changes(seed)
