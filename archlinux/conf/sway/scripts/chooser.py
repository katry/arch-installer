#!/usr/bin/env python
from time import sleep
from os import listdir
from os.path import isfile, join
from threading import Thread
from i3ipc import Connection, Event
from json import loads
from evdev import InputDevice
from select import select
from asyncio import get_event_loop
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gdk
from setproctitle import setproctitle

setproctitle("window-choose-list")


class ChooserWindow(Gtk.Window):
    def __init__(self, items, app_id, con_id, controller):
        super().__init__(title="")
        self.set_border_width(0)
        self.controller = controller

        box_outer = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(box_outer)

        self.listbox = Gtk.ListBox()
        self.listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        box_outer.pack_start(self.listbox, True, True, 0)
        css = b"""
            row { background-color: #000000; }
            row.active { background-color: #285577; }
        """
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(css)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        self.items = items
        self.rows = {}
        self.focused = None
        self.load_icon(app_id)
        self.show(con_id)

    def load_icon(self, app_id):
        mapper = {
            "subl": "sublime_text",
        }
        icon_mapper = {
            "tilix": "tilix",
            "Ferdium": "ferdium"
        }
        app = app_id
        if app[0:4] == "Gimp":
            app = "gimp"
        if app_id in mapper:
            app = mapper[app_id]

        prefix = "/usr/share/applications/"
        files = {
            f.lower().replace(".desktop", ""): f for f in listdir(prefix) if isfile(join(prefix, f))
        }
        if app in icon_mapper:
            self.icon = icon_mapper[app]
            return
        self.icon = ""
        if app.lower() in files:
            df = open(prefix + files[app.lower()], "r")
            while line := df.readline():
                pl = line.split("=")
                if pl[0] in ["icon", "Icon"]:
                    self.icon = pl[1].replace("\n", "")
            df.close()

    def show(self, con_id):
        if not self.controller.pressed:
            return
        for i in self.items:
            label = Gtk.Label(label="  %s" % self.items[i]["name"], xalign=0)
            image = Gtk.Image()
            image.set_from_icon_name(self.icon, 32)
            row = Gtk.ListBoxRow()
            grid = Gtk.Grid()
            grid.add(image)
            grid.add(label)
            row.add(grid)
            self.listbox.add(row)
            if con_id == self.items[i]["id"]:
                Gtk.StyleContext.add_class(row.get_style_context(), "active")
                self.active = row
            self.rows[i] = row

    def update(self, con_id):
        for id in self.rows:
            Gtk.StyleContext.remove_class(self.rows[id].get_style_context(), "active")
        if con_id in self.rows:
            Gtk.StyleContext.add_class(self.rows[con_id].get_style_context(), "active")


class Controller:
    def __init__(self):
        self.i3 = Connection()
        self.items = {}
        self.app_id = ""
        self.window = None
        self.thread = None
        self.pressed = True

        t = Thread(target=self.listen)
        t.start()

        self.__loop = get_event_loop()
        self.__loop.run_until_complete(self.main())

    def listen(self):
        dev = InputDevice('/dev/input/by-path/platform-i8042-serio-0-event-kbd')
        while True:
            select([dev], [], [])
            for event in dev.read():
                if event.code == 56 and event.value == 0:  # ALT UP
                    self.pressed = False
                    self.hide()
                elif event.code == 56 and event.value != 0:  # ALT DOWN
                    self.pressed = True
                elif event.code != 41 and e.code != 4:
                    self.pressed = False

    async def main(self):
        self.i3.on(Event.WINDOW_FOCUS, self.process)
        while self.pressed:
            await self.i3.main()

    def process(self, i3, e):
        focused = self.i3.get_tree().find_by_id(e.container.ipc_data["id"])
        if focused.app_id in ["chooser.py", "tilix-dropdown", "network_launcher", "menu_launcher"]:
            return
        if not self.items:
            self.load(focused.workspace(), focused)
        app_id = focused.app_id or focused.window_class
        id = focused.id
        if self.app_id == app_id and self.window:
            if self.pressed:
                return self.update(id)
        else:
            if self.window:
                self.hide()
                self.load(focused.workspace(), focused)
            self.thread = Thread(target=self.show, args=())
            self.thread.start()
            if not self.pressed:
                return
            while not self.window:
                if not self.pressed:
                    return
                sleep(0.05)
                continue
            self.i3.command("for_window [app_id=\"chooser.py\"] opacity set 0")
            focused.command("focus")
            height = 29 * len(self.ids)
            sleep(0.05)
            cmd = "[app_id=\"chooser.py\"] resize set height %s px"
            i3.command(cmd % height)

            def opacity():
                sleep(0.3)
                self.i3.command("[app_id=\"chooser.py\"] opacity set 1")
                focused.command("focus")
            t = Thread(target=opacity, args=())
            t.start()
            return True

    def load(self, workspace, focused):
        self.items = {}
        self.focused = focused.id
        self.app_id = focused.app_id or focused.window_class
        self.workspace = workspace
        self.output = workspace.ipc_data["output"]
        self.ids = []
        try:
            fd = open("/tmp/.tab-switcher-status", "r")
            data = loads(fd.read())
            fd.close()
        except Exception:
            return
        for w in data[self.output][str(workspace.id)]["windows"][self.app_id]:
            self.ids.append(w)
            item = workspace.find_by_id(w)
            self.items[w] = {
                "name": item.name,
                "app_id": self.app_id,
                "id": w,
            }

    def on_click(self, window, event):
        i3 = Connection()
        tree = i3.get_tree()
        tree.find_by_id(self.focused).command("focus")

    def show(self):
        if self.pressed:
            self.window = ChooserWindow(
                self.items, self.app_id, self.focused, self
            )
            self.window.connect('button-press-event', self.on_click)
            self.window.connect("destroy", Gtk.main_quit)
            self.window.show_all()
            Gtk.main()

    def update(self, id=None):
        if id:
            self.focused = id
        else:
            try:
                index = self.ids.index(self.focused)
                self.focused = self.focused + 1 if index + 1 < len(self.ids) else 0
            except Exception:
                return False
        self.window.update(self.focused)
        return True

    def hide(self):
        if not self.window:
            return
        try:
            self.window.destroy()
        except Exception:
            pass
        self.focused = None
        self.items = {}
        self.app_id = None
        self.window = None


c = Controller()
