#!/usr/bin/env python
from json import dumps
from i3ipc import Connection


if __name__ == "__main__":
	i3 = Connection()
	focused = i3.get_tree().find_focused()
	invalid = [
		"tilix-dropdown",
		"network_launcher",
		"menu_launcher",
		"menu_launcher",
		"menu_launcher",
		"menu_launcher",
		"window-choose-list",
		"chooser.py",
	]
	if focused.type == "con" and focused.app_id not in invalid:
		print(dumps({"text": "Closable", "percentage": 100}))
	else:
		print(dumps({"text": "Non-closable", "percentage": 0}))
