#!/bin/sh

dconf load / < ~/.config/sway/first_boot.conf
rm -f ~/.config/sway/first_boot.conf

# Docker db images prepare
cd ~/.custom_images/postgres
docker-compose build
docker-compose create
cd ~/.custom_images/mysql
docker-compose build
docker-compose create
cd ~


# sudo rm -rf /etc/sudoers2
# sudo echo "%wheel ALL=(ALL) ALL" > ./sudoers2
# sudo chown 0 ./sudoers2
# sudo mv ./sudoers2 /etc/sudoers

rm -rf /first_boot.sh
sed -i "s#exec foot -e /first_boot.sh##" ~/.config/sway/config
reboot
