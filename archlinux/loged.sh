#!/bin/sh

cd ~
mkdir Work
mkdir Downloads
mkdir Documents
mkdir Experiments
mkdir Music
mkdir Pictures
mkdir Projects
mkdir .custom_images
mkdir .config/gtk-4.0

cp -rf /conf/custom_images/* ./.custom_images/

git config --global core.editor nano
git config --global user.email "${3}"
git config --global user.name "${2}"

if [ "${1}" = "gnome" ]; then
    cp -rf /conf/gnome/display_modes.sh ~/.config/display_modes.sh
    cp -rf /conf/gnome/monitors_gaming.xml ~/.config/monitors_gaming.xml
    cp -rf /conf/gnome/monitors_work.xml ~/.config/monitors_work.xml
    cp -rf /conf/gnome/gaming_sink ~/.config/gaming_sink
    cp -rf /conf/gnome/work_sink ~/.config/work_sink
    cp -rf /conf/gnome/Templates ~/Templates
    mkdir ~/.mozilla
    cp -rf /conf/firefox ~/.mozilla/firefox
fi


cp -rf /conf/lilith.png ~/Pictures/
cp -rf /conf/dircolors ~/.dircolors


mkdir -p ~/.config/zed/
cp -rf /conf/zed/* ~/.config/zed/

npm config set ignore-scripts true

mkdir ~/.config/sublime-text
cp -rf /conf/sublime/* ~/.config/sublime-text/
alias yay='yay --mflags --skipinteg --cleanafter --removemake  --noconfirm'

cd /tmp
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions

yay -S authbind
yay -S pam-fprint-grosshack
yay -S coppwr # pipewire gui client (solving problems with sound passthrough on LG TV?)

yay -S wasmtime-bin binaryen-git assemblyscript
yay -S sublime-text-4
yay -S blender-bin

yay -S minecraft-launcher

if [ "${1}" = "sway" ]; then
    yay -S greetd greetd-gtkgreet
    yay -S redshift-wayland-git
    mkdir ~/.mozilla
    cp -rf /conf/firefox ~/.mozilla/firefox
    pip install pywayland pywlroots i3ipc setproctitle evdev psutil
fi

if [ "${4}" = "amd" ]; then
    if [ "${5}" = "2" ]; then
        yay -S zenmonitor
    else
        yay -S zenmonitor3-git
    fi
    yay -S zenpower-dkms
    yay -S ryzenadj-git
    yay -S amdctl
fi

cd
/opt/kite/kite-installer install

git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions

if [ "${1}" = "gnome" ]; then
    yay -S flat-remix-gnome
    yay -S flat-remix-gtk
    yay -S flat-remix
    # cd /tmp
    # git clone https://github.com/RensAlthuis/vertical-overview.git
    # cd vertical-overview
    # make
    # make install

    # wayland window session manager:
    # https://github.com/nlpsuge/gnome-shell-extension-another-window-session-manager

    # cd /tmp
    # git clone https://gitlab.com/arcmenu/ArcMenu.git
    # cd ArcMenu
    # make install

    # cd /tmp
    # git clone https://gitlab.com/AndrewZaech/aztaskbar.git
    # cd aztaskbar
    # make install

fi
