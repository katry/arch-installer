#!/bin/sh

ln -sf /usr/share/zoneinfo/Europe/Prague /etc/localtime
hwclock --systohc
echo "cs_CZ.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=cs_CZ.UTF-8" >> /etc/locale.conf
echo "LANGUAGE=cs_CZ:en_US" >> /etc/locale.conf
echo "KEYMAP=cz-us-qwertz" > /etc/vconsole.conf
echo "${10}" > /etc/hostname
echo 'ALL_config="/etc/mkinitcpio.conf"' > /etc/mkinitcpio.d/linux-lts.preset
echo "ALL_kver=\"/boot/vmlinuz-${3}\"" >> /etc/mkinitcpio.d/linux-lts.preset
echo "PRESETS=('default' 'fallback')" >> /etc/mkinitcpio.d/linux-lts.preset
echo "default_image=\"/boot/initramfs-${3}.img\"" >> /etc/mkinitcpio.d/linux-lts.preset
echo "fallback_image=\"/boot/initramfs-${3}-fallback.img\"" >> /etc/mkinitcpio.d/linux-lts.preset
echo 'fallback_options="-S autodetect"' >> /etc/mkinitcpio.d/linux-lts.preset
cp /etc/mkinitcpio.d/linux-lts.preset /etc/mkinitcpio.d/linux.preset -rf
echo "[multilib]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf


pacman --noconfirm -Syu

if [ "${9}" = "nvidia" ]; then
    pacman  --noconfirm -S lib32-opencl-nvidia lib32-nvidia-utils nvidia-lts opencl-nvidia libvdpau lib32-libvdpau
    ln -s /dev/null /etc/udev/rules.d/61-gdm.rules
    echo "MUTTER_DEBUG_KMS_THREAD_TYPE=user" >> /etc/environment 
elif [ "${9}" = "amd" ]; then
    pacman  --noconfirm -S mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau ib32-mesa-vdpau radeontop
elif [ "${9}" = "intel" ]; then
    pacman  --noconfirm -S vulkan-intel libva-intel-driver xf86-video-intel
fi

if [ "${14}" = "amd" ]; then
    pacman --noconfirm -S amd-ucode
fi

pacman --noconfirm -S zsh openssh openssl chromium firefox networkmanager wget nano flatpak \
    python python-pip python-virtualenv python-pyudev python-setuptools uv \
    rust maturin corrosion cargo-asm cargo-audit cargo-edit cargo-feature cargo-fuzz \
    cargo-outdated cargo-watch rust-musl rust-src rust-wasm python-setuptools-rust nmap htop \
    ecryptfs-utils wayland rsync lsof dialog gstreamer steam archlinux-keyring git firewalld \
    imagemagick cpupower ntfs-3g unrar zip unzip openvpn networkmanager-openvpn docker \
    docker-compose nodejs flac tor nyx certbot clang jdk-openjdk net-tools picocom sshfs \
    inkscape hwinfo bc expac dpkg bluez bluez-utils npm jq direnv gendesk cronie pacman-contrib \
    android-tools noto-fonts noto-fonts-emoji ttf-opensans ttf-fira-sans ttf-linux-libertine \
    ttf-dejavu ttf-croscore dfu-util gperf esptool arduino arduino-cli screen fprintd \
    evtest element-desktop xdg-user-dirs-gtk zed helix

pacman -Rns --noconfirm vi

npm config set ignore-scripts true
npm install -g prettier typescript vercel eslint jshint

sed -i "s/-march=x86-64 -mtune=generic/-march=native/" /etc/makepkg.conf


groupadd plugdev
echo "KERNEL==\"hidraw*\", SUBSYSTEM==\"hidraw\", MODE=\"0664\", GROUP=\"plugdev\"" > /etc/udev/rules.d/99-hidraw.rules
useradd -m --groups wheel,audio,disk,input,kvm,optical,scanner,storage,video,plugdev -s /bin/zsh ${2}
echo "Seting user password:"
if [ ! -z "${6}" ]; then
    echo "${2}:${6}" | chpasswd
else
    passwd ${2}
fi

mv /etc/sudoers /etc/sudoers2
echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers
cd /home/${2}
su ${2} -c "cd ~; git clone https://aur.archlinux.org/yay.git"
su ${2} -c "cd ~/yay; makepkg -sri --noconfirm"
rm /home/${2}/yay -rf
cd /

if [ "${8}" = "gnome" ]; then
    pacman --noconfirm -S gnome-shell xorg-server-xwayland egl-wayland libinput mutter\
    network-manager-applet  gnome-calendar mplayer ipset nemo libsecret fractal evolution \
    file-roller eog gdm gnome-screenshot tilix gvfs gvfs-mtp gnome-keyring \
    solaar gnome-shell-extensions dconf gnome-tweaks dconf-editor \
    gnome-mplayer gmtk gparted gnome-text-editor libgtop glib2 ydotool procps-ng \
    gnome-control-center gnome-browser-connector gnome-boxes lutris kooha \
    qemu-desktop qemu-block-gluster qemu-user-static qemu-hw-display-virtio-gpu \
    qemu-hw-display-virtio-vga-gl qemu-hw-display-virtio-gpu-pci qemu-hw-display-virtio-gpu-pci-gl \
    qemu-vhost-user-gpu
    systemctl enable gdm.service

    cp /conf/gnome/tilix_quake.sh /etc/tilix_quake.sh

    mkdir -p /home/${2}/.config/gtk-4.0/
    mkdir /home/${2}/.local
    mkdir /home/${2}/.local/share
    mkdir /home/${2}/.local/share/gnome-shell
    mkdir /home/${2}/.config

    chown ${2} /home/${2}/.local -R
    chmod u+rw /home/${2}/.local -R
    chown ${2} /home/${2}/.config -R
    chmod u+rw /home/${2}/.config -R
    chown ${2} /home/${2}/.icons -R
    chmod u+rw /home/${2}/.icons -R
    chown ${2} /home/${2}/.themes -R
    chmod u+rw /home/${2}/.themes -R

    cp -rf /conf/gnome/firstrun.sh /first_boot.sh
    mkdir -p /home/${2}/.config/autostart
    cp -rf /conf/gnome/first_boot-autostart.desktop /home/${2}/.config/autostart/first_boot-autostart.desktop
    chown ${2} /first_boot.sh
    chown ${2} /home/${2}/.config/autostart/first_boot-autostart.desktop
    chmod +x /first_boot.sh

    cp /conf/gnome/gnome.conf /home/${2}/.gnome.conf
    chown ${2} /home/${2}/.gnome.conf

    sed "/^\[daemon\]$/a AutomaticLogin=${2}" /etc/gdm/custom.conf > /buffer
    mv ./buffer /etc/gdm/custom.conf
    sed "/^\[daemon\]$/a AutomaticLoginEnable=True" /etc/gdm/custom.conf > /buffer
    mv ./buffer /etc/gdm/custom.conf
fi

cp -rf /conf/bashrc /home/${2}/.bashrc
cp -rf /conf/zshrc /home/${2}/.zshrc
cp -rf /conf/profile /home/${2}/.profile
cp -rf /conf/inputrc /home/${2}/.inputrc


mkdir /home/${2}/.zsh
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh -O /home/${2}/.zsh/_git


chown ${2} /home/${2}/.bashrc
chown ${2} /home/${2}/.zshrc
chown ${2} /home/${2}/.zsh
chown ${2} /home/${2}/.zsh/_git
chown ${2} /home/${2}/.profile
chown ${2} /home/${2}/.inputrc

cp -f /conf/bashrc /root/.bashrc
cp -f /conf/profile /root/.profile
cp -f /conf/dircolors /root/.dircolors


systemctl enable NetworkManager
systemctl enable docker.service
systemctl enable firewalld.service
systemctl enable bluetooth.service
systemctl enable cronie.service
systemctl enable tor.service
systemctl enable sshd.service
mkdir /home/${2}/.ssh
echo "AddKeysToAgent yes" >> /home/${2}/.ssh/config
echo "ForwardAgent yes" >> /home/${2}/.ssh/config
echo "" >> /home/${2}/.ssh/config
chown ${2} /home/${2}/.ssh -R

gpasswd -a ${2} docker
chmod +x /loged.sh
echo "Setting root password:"
if [ ! -z "${7}" ]; then
    echo "root:${7}" | chpasswd
else
    passwd
fi

# gpasswd -a ${2} adbusers
gpasswd -a ${2} plugdev
gpasswd -a ${2} tor
# cp -rf /conf/android.rules /etc/udev/rules.d/51-android.rules

su ${2} -c "/loged.sh ${8} ${2} ${13} ${14} ${15}"

if [ "${11}" = "YES" ]; then
    pacman --noconfirm -S amd-ucode grub
    pacman --noconfirm -S mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau \
    lib32-mesa-vdpau xf86-video-amdgpu vulkan-intel libva-intel-driver xf86-video-intel lib32-opencl-nvidia lib32-nvidia-utils \
    nvidia-lts opencl-nvidia libvdpau lib32-libvdpau
    cp -f /conf/mkinitcpio.conf /etc/mkinitcpio.conf
    mkinitcpio -p linux
    if [ ! -z "${12}" ]; then
        grub-install --target=i386-pc ${12} --recheck
    fi
    grub-install --target=x86_64-efi --efi-directory=/boot --removable --recheck
    grub-mkconfig -o /boot/grub/grub.cfg
else
    if [ "${4}" != "NO" ]; then
        bootctl --path=/boot install
        echo "bootloader installed"
        ls /boot
        echo "default arch${3}" > /boot/loader/loader.conf
        echo "timeout 4" >> /boot/loader/loader.conf
        echo "console-mode max" >> /boot/loader/loader.conf
    fi
    blkid -s PARTUUID | grep ${1} | tr "\"" "\n" | grep "-" > ./buff.txt
    A=`cat buff.txt`

    echo "title	ArchLinux" > /boot/loader/entries/arch${3}.conf
    echo "linux	/vmlinuz-${3}" >> /boot/loader/entries/arch${3}.conf
    echo "initrd	/initramfs-${3}.img" >> /boot/loader/entries/arch${3}.conf
    echo "options root=PARTUUID=${A} rw"  >> /boot/loader/entries/arch${3}.conf
    rm -f ./buff.txt
    mkinitcpio -p linux
    if [ ! -z "${17}" ]; then
        mkdir /tmp/winefi
        mount ${17} /tmp/winefi
        cp -r /tmp/winefi/EFI/Microsoft /boot/EFI/
        umount ${17}
    fi
fi

paccache -ruk0
rm -rf /var/log/*
rm -rf /var/cache/*
rm -rf /var/lib/systemd/coredump/*

if [ "${16}" == "YES" ]; then
    cp -rf /conf/pam/* /etc/pam.d/
    ls /conf/pam
    echo "fingerprint PAM copy done"
fi

# serial devices user access rights:
# esp32-c3
echo "ATTRS{idVendor}==\"1a86\", ATTRS{idProduct}==\"7523\", SYMLINK+=\"ttyUSB0\", GROUP=\"${2}\", MODE=\"0660\"" >> /etc/udev/rules.d/50-usb-serial.rules
# raspberry pi pico
echo "ATTRS{idVendor}==\"2e8a\", ATTRS{idProduct}==\"0005\", SYMLINK+=\"ttyACM0\", GROUP=\"${2}\", MODE=\"0660\"" >> /etc/udev/rules.d/50-usb-serial.rules

# HOME directory encrypt preparation
# modprobe ecryptfs
# ecryptfs-migrate-home -u {2}
# echo -e "ls" /empty.sh
# chmod a+x /empty.sh
# su ${2} -c /empty.sh

# sed -i "/^auth *\[default=die\] *pam_faillock.so *authfail/aauth       [success=1 default=ignore]  pam_succeed_if.so service = systemd-user quiet\nauth       required                    pam_ecryptfs.so unwrap" /etc/pam.d/system-auth
# sed -i "/^-password/apassword   optional                    pam_ecryptfs.so" /etc/pam.d/system-auth
# sed -i "/^session *required *pam.unix.so/asession    [success=1 default=ignore]  pam_succeed_if.so service = systemd-user quiet\nsession    optional                    pam_ecryptfs.so unwrap" /etc/pam.d/system-auth
# su ${2} -c /empty.sh
# rm /empty.sh
# rm -rf /home/${2}.*

echo "Installation DONE!"
