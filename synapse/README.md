# Synapse installator

contains:
- synapse server
- nginx server
- certbot automtic certificate generation
- messenger bridge
- telegram bridge
- signal bridge
- slack bridge

## Example
```sh
./install.sh --dir /home/katry/synapse --admin katry --domain matrix.katryapps.com --telegram_id "telegram_id" --telegram_hash "telegram_hash"
```
(get telegram credentials from https://my.telegram.org/apps)
