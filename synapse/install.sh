 #!/bin/sh

ARGUMENT_LIST=(
    "dir"
    "domain"
    "admin"
    "password"
    "postgres"
    "upgrade"
    "telegram_id"
    "telegram_hash"
)
opts=$(getopt \
    --longoptions "$(printf "%s:," "${ARGUMENT_LIST[@]}")" \
    --name "$(basename "$0")" \
    --options "" \
    -- "$@"
)
eval set --$opts
while [[ $# -gt 0 ]]; do
    case $1 in
        --dir)
            dir=$2
            shift 2
            ;;

        --domain)
            domain=$2
            shift 2
            ;;

        --admin)
            admin=$2
            shift 2
            ;;

        --password)
            password=$2
            shift 2
            ;;

        --postgres)
            postgres=$2
            shift 2
            ;;

        --telegram_id)
            telegram_id=$2
            shift 2
            ;;
        --telegram_hash)
            telegram_hash=$2
            shift 2
            ;;

        --upgrade)
            upgrade="Y"
            shift 1
            ;;
        *)
            break
            ;;
    esac
done

if [ `whoami` != "root" ]; then
    echo "Run this script as root."
    exit
fi
bd=`pwd`

if [[ -z "${dir}" ]]; then
    dir="/synapse"
fi

mkdir -p ${dir}

echo "Create synapse in directory ${dir}"

cp -r ./bridges ${dir}
cp -r ./services ${dir}
cp ./nginx.conf ${dir}

cd ${dir}

if [[ -z "${admin}" ]]; then
    admin="admin"
fi

if [[ ! -z "${upgrade}" ]]; then
    source venv/bin/activate
    python3 -m pip install --upgrade matrix-synapse mautrix-facebook[all] mautrix-telegram[all] mautrix-signal[all]
    cd ./mautrix_slack
    git pull
    ./build.sh -tags nocrypto
    cd ..
    shutdown -r 0
else
    if [ -f "/etc/arch-release" ]; then
        pacman --noconfirm -S certbot python-pip python-virtualenv python-setuptools nginx certbot-nginx go git libolm
    else
        wget -O /usr/share/keyrings/signald.gpg https://signald.org/signald.gpg
        echo "deb [signed-by=/usr/share/keyrings/signald.gpg] https://updates.signald.org unstable main" > /etc/apt/sources.list.d/signald.list
        apt update
        apt-get install python3-pip python3-virtualenv python3-setuptools python3-certbot-nginx nginx certbot golang git signald -y
    fi
    python3 -m venv venv
    source venv/bin/activate

    python3 -m pip install --upgrade matrix-synapse mautrix-facebook[all] mautrix-telegram[all] mautrix-signal[all]

    # certbot
    killall nginx
    certbot --nginx -d ${domain}
    killall nginx
    cp ./nginx.conf /etc/nginx/nginx.conf
    sed -i "s/__DOMAIN__/${domain}/g" /etc/nginx/nginx.conf

    python -m synapse.app.homeserver \
    --server-name ${domain} \
    --config-path homeserver.yaml \
    --generate-config \
    --report-stats=no

    # update path in services

    # generate registrations
    sed -i "s/__USER__/${admin}/g" ./bridges/*
    sed -i "s/__DOMAIN__/${domain}/g" ./bridges/*
    sed -i "s|__PATH__|${dir}|g" ./bridges/*
    sed -i "s/__TELEGRAM_ID__/${telegram_id}/g" ./bridges/*
    sed -i "s/__TELEGRAM_HASH__/${telegram_hash}/g" ./bridges/*

    if [[ ! -z "${postgres}" ]]; then
        userpass="$(echo ${postgres} | grep @ | cut -d@ -f1)"
        dbuser=`echo "$userpass" | grep : | cut -d: -f1`
        dbpass=`echo "$userpass" | grep : | cut -d: -f2`
        hostport="$(echo ${postgres/$userpass@/} | cut -d/ -f1)"
        dbhost="$(echo $hostport | sed -e 's,:.*,,g')"
        dbport="$(echo $hostport | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')"
        db="$(echo ${postgres} | grep / | cut -d/ -f2-)"

        sed -i "s/sqlite3/psycopg2\n  txn_limit: 10000/g" ./homeserver.yaml
        sed -i "s/database: .\+/database: ${postgres}/g" ./homeserver.yaml
        sed -i "s/database: .\+/user:
        ${dbuser}\n    password: ${dbpass}\n    database: ${db}\n    host: ${dbhost}\n    port: ${dbport}\n    cp_min: 5\n    cp_max: 10/g"
        sed -i "s/sqlite3-fk-wal/postgres/g" ./bridges/*
        sed -i "s|sqlite:///homeserver.db?_txlock=immediate|${postgres}|g" ./bridges/*
        sed -i "s|sqlite:///homeserver.db|${postgres}|g" ./bridges/*
    fi

    python -m mautrix_facebook -c ./bridges/messenger_config.yaml -r ./bridges/messenger_registration.yaml -g

    python -m mautrix_telegram -c ./bridges/telegram_config.yaml -r ./bridges/telegram_registration.yaml -g

    python -m mautrix_signal -c ./bridges/signal_config.yaml -r ./bridges/signal_registration.yaml -g

    git clone https://github.com/mautrix/slack.git mautrix_slack
    cd ./mautrix_slack
    ./build.sh -tags nocrypto
    cd ..

    ./mautrix_slack/mautrix-slack -c ./bridges/slack_config.yaml -r ./bridges/slack_registration.yaml -g

    # create systemd services
    sed -i "s|__PATH__|${dir}|g" ./services/*
    cp ./services/* /etc/systemd/system/

    # enable systemd services
    systemctl enable synapse.service
    systemctl enable mautrix_messenger.service
    systemctl enable mautrix_telegram.service
    systemctl enable mautrix_signal.service
    systemctl enable mautrix_slack.service
    systemctl enable nginx.service
    systemctl enable signald.service

    systemctl start nginx.service
    systemctl start synapse.service
    sleep 10
    if [[ ! -z "${password}" ]]; then
        register_new_matrix_user -u ${admin} -p ${password} -c ./homeserver.yaml -a
    else
        register_new_matrix_user -u ${admin} -c ./homeserver.yaml -a
    fi

    # bridge links into main config
    echo "" >> ./homeserver.yaml
    echo "app_service_config_files:" >> ./homeserver.yaml
    echo "- ${dir}/bridges/messenger_registration.yaml" >> ./homeserver.yaml
    echo "- ${dir}/bridges/telegram_registration.yaml" >> ./homeserver.yaml
    echo "- ${dir}/bridges/signal_registration.yaml" >> ./homeserver.yaml
    echo "- ${dir}/bridges/slack_registration.yaml" >> ./homeserver.yaml

    echo "In root crontab create rule:"
    echo "0 1 * * *     certbot renew"
    echo "Then reboot"
    cd $bd
fi
