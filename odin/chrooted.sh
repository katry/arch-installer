
ln -sf /usr/share/zoneinfo/${REGION}/${CITY} /etc/localtime
echo "cs_CZ.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=cs_CZ.UTF-8" > /etc/locale.conf
echo "KEYMAP=cz-us-qwertz" > /etc/vconsole.conf

sed -i s/#ParallelDownloads/ParallelDownloads/g /etc/pacman.conf
sed -i s/#COMPRESSION=\"zstd\"/COMPRESSION=\"zstd\"/g /etc/mkinitcpio.conf
sed -i s/PRESETS=(\'default\' \'fallback\')/PRESETS=(\'default\')/g /etc/mkinitcpio.d/linux-aarch64.preset

pacman-key --init
pacman-key --populate archlinuxarm

echo ${HOSTNAME} > /etc/hostname

usermod -l ${USERNAME} alarm
usermod -d /home/${USERNAME} -m ${USERNAME}
echo "${USERNAME}:${PASSWORD}" | chpasswd

mv /etc/sudoers /etc/sudoers2
echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers
cd /home/${USERNAME}
su ${USERNAME} -c "cd ~; git clone https://aur.archlinux.org/yay.git"
su ${USERNAME} -c "cd ~/yay; makepkg -sri --noconfirm"
rm /home/${USERNAME}/yay -rf
cd /

pacman --noconfirm -Syu
pacman --noconfirm -S base-devel git firefox chromium wireless-regdb clang compiler-rt zsh openssh \
	openssl wget curl nano flatpak python python-pip python-virtualenv mypy python-pylint \
	python-jedi jedi-language-server flake8 python-flake8-docstrings python-flake8-isort \
	python-pyudev python-coverage nmap htop ecryptfs-utils wayland rsync lsof gstreamer \
	firewalld imagemagick cpupower ntfs-3g unrar zip unzip openvpn archlinux-keyring \
	networkmanager-openvpn flac tor nyx certbot clang jdk-openjdk net-tools picocom sshfs \
	inkscape expac dpkg bluez bluez-utils npm jq cronie pacman-contrib noto-fonts \
	noto-fonts-emoji ttf-opensans ttf-fira-sans ttf-linux-libertine ttf-dejavu \
	ttf-croscore dfu-util gperf screen fprintd evtest xdg-user-dirs

hostnamectl chassis handset

pacman --noconfirm -S gnome-shell xorg-server-xwayland egl-wayland libinput mutter \
	network-manager-applet gnome-calendar mplayer ipset nemo libsecret \
	file-roller eog gdm gnome-screenshot tilix gvfs-google gvfs-goa gvfs-gphoto2 gvfs \
	gvfs-mtp gnome-keyring solaar gnome-shell-extensions dconf gnome-tweaks dconf-editor \
	gnome-mplayer gmtk gparted gnome-text-editor libgtop glib2 ydotool procps-ng \
	gnome-control-center gnome-browser-connector kooha

systemctl enable gdm.service

cp -rf /conf/bashrc /home/${USERNAME}/.bashrc
cp -rf /conf/zshrc /home/${USERNAME}/.zshrc
cp -rf /conf/profile /home/${USERNAME}/.profile
cp -rf /conf/inputrc /home/${USERNAME}/.inputrc

mkdir /home/${USERNAME}/.zsh
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh -O /home/${USERNAME}/.zsh/_git

chown ${USERNAME} /home/${USERNAME}/.bashrc
chown ${USERNAME} /home/${USERNAME}/.zshrc
chown ${USERNAME} /home/${USERNAME}/.zsh
chown ${USERNAME} /home/${USERNAME}/.zsh/_git
chown ${USERNAME} /home/${USERNAME}/.profile
chown ${USERNAME} /home/${USERNAME}/.inputrc

systemctl disable systemd-networkd
systemctl enable NetworkManager
systemctl enable firewalld.service
systemctl enable bluetooth.service
systemctl enable cronie.service
systemctl enable tor.service
systemctl enable sshd.service

pacman -U /conf/mesa-git-24.0.0_devel.179592.ce5475366e9.d41d8cd-1-aarch64.pkg.tar.xz

echo "__GLX_VENDOR_LIBRARY_NAME=mesa" > /etc/environment
echo "MESA_LOADER_DRIVER_OVERRIDE=zink" >> /etc/environment
echo "GALLIUM_DRIVER=zink" >> /etc/environment

cp -r /conf/firmware/* /mnt/lib/firmware/
sed -i s/FILES()/"#FILES()"/g /etc/mkinitcpio.conf
echo "FILES=(" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/qcom/sm8550/ayn/odin2/adsp.mbn" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/qcom/sm8550/ayn/odin2/adsp_dtb.mbn" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/qcom/sm8550/ayn/odin2/cdsp.mbn" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/qcom/sm8550/ayn/odin2/cdsp_dtb.mbn" >> /etc/mkinitcpio.conf

echo "        /lib/firmware/ath12k/WCN7850/hw2.0/amss.bin" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/ath12k/WCN7850/hw2.0/regdb.bin" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/ath12k/WCN7850/hw2.0/board-2.bin" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/ath12k/WCN7850/hw2.0/m3.bin" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/qca/hmtbtfw20.tlv" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/qca/hmtnv20.bin" >> /etc/mkinitcpio.conf

echo "        /lib/firmware/qcom/sm8550/ayn/odin2/a740_zap.mbn" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/qcom/gmu_gen70200.bin" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/qcom/a740_sqe.fw" >> /etc/mkinitcpio.conf

echo "        /lib/firmware/regulatory.db.p7s" >> /etc/mkinitcpio.conf
echo "        /lib/firmware/regulatory.db" >> /etc/mkinitcpio.conf
echo "        )" >> /etc/mkinitcpio.conf
mkinitcpio -P

cd /tmp
git clone https://github.com/andersson/qrtr.git
git clone https://github.com/andersson/pd-mapper.git
git clone https://github.com/andersson/tqftpserv.git

pushd qrtr && make prefix=/usr && make prefix=/usr install && systemctl enable qrtr-ns && popd
pushd pd-mapper && make prefix=/usr && make prefix=/usr install && systemctl enable pd-mapper && popd
pushd tqftpserv && make prefix=/usr && make prefix=/usr install && systemctl enable tqftpserv && popd
cd /

mkdir /home/${USERNAME}/.ssh
echo "AddKeysToAgent yes" >> /home/${USERNAME}/.ssh/config
echo "ForwardAgent yes" >> /home/${USERNAME}/.ssh/config
echo "" >> /home/${USERNAME}/.ssh/config
chown ${USERNAME} /home/${USERNAME}/.ssh -R

echo "Setting root password:"
if [ ! -z "${ROOT_PASSWORD}" ]; then
	echo "root:${ROOT_PASSWORD}" | chpasswd
else
	passwd
fi

gpasswd -a ${USERNAME} plugdev
gpasswd -a ${USERNAME} tor

su ${USERNAME} -c "/loged.sh"

paccache -ruk0
rm -rf /var/log/*
rm -rf /var/cache/*
rm -rf /var/lib/systemd/coredump/*

# DISABLE gamepad mouse
echo 'SUBSYSTEM=="input", ATTRS{name}=="Ayn Odin2 Gamepad", MODE="0666", ENV{ID_INPUT_MOUSE}="0", ENV{ID_INPUT_JOYSTICK}="1"' > /etc/udev/rules.d/99-ignore-gamepad.rules
